package com.gvan.mapstest.ui.main.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gvan.mapstest.data.local_data.LocalDataManager
import com.gvan.mapstest.data.model.Field
import com.gvan.mapstest.data.repository.fields.FieldsRepository
import com.gvan.mapstest.ui.main.model.FieldsData
import com.gvan.mapstest.utils.SingleLiveEvent
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(private val fieldsRepository: FieldsRepository,
                                        private val localDataManager: LocalDataManager) : ViewModel() {

    private val allFields : MutableList<Field> = arrayListOf()
    private var filter: String = ""

    val fieldsLiveData = MutableLiveData<FieldsData>()
    val openFieldLiveData = SingleLiveEvent<Field>()
    val showProgressLiveData = SingleLiveEvent<Boolean>()

    fun getFields() {
        showProgressLiveData.value = true
        viewModelScope.launch {
            fieldsRepository.getFields(localDataManager.getDevId())
                .catch {
                    showProgressLiveData.value = false
                    fieldsLiveData.value = FieldsData(arrayListOf(), false)
                }
                .collect { fields ->
                    showProgressLiveData.value = false
                    allFields.clear()
                    allFields.addAll(fields)
                    val resultFields = applyFilter(allFields, filter)
                    fieldsLiveData.value = FieldsData(resultFields, false)
                }
        }
    }

    fun onFieldClicked(field: Field){
        openFieldLiveData.value = field
    }

    private fun applyFilter(list:List<Field>, filter: String) : List<Field> {
        return list.filter { field -> field.FieldNo.toLowerCase().contains(filter) or
                field.FieldNoDescr.toLowerCase().contains(filter) or
                field.CornType.toLowerCase().contains(filter)}
    }

    fun onFilterChanged(filter: String?) {
        this.filter = (filter ?: "").toLowerCase()
        val resultFields = applyFilter(allFields, this.filter)
        fieldsLiveData.value = FieldsData(resultFields, true)
    }

}