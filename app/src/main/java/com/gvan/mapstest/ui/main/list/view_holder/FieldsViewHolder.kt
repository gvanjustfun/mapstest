package com.gvan.mapstest.ui.main.list.view_holder

import androidx.recyclerview.widget.RecyclerView
import com.gvan.mapstest.R
import com.gvan.mapstest.data.model.Field
import com.gvan.mapstest.databinding.ItemFieldBinding
import java.text.DecimalFormat

class FieldsViewHolder (private val binding: ItemFieldBinding) : RecyclerView.ViewHolder(binding.getRoot()) {

    fun bind(field: Field, listener: (Field) -> Unit){
        binding.fieldNo.text = field.FieldNo
        binding.fieldNoDescr.text = field.FieldNoDescr
        binding.cornType.text = field.CornType

        val area = DecimalFormat("##0.00").format(field.Area)
        binding.area.text = binding.root.context.getString(R.string.ga, area.toString())

        binding.root.setOnClickListener { _ -> listener(field) }
    }

}