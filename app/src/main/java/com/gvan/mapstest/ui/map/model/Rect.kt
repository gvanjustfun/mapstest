package com.gvan.mapstest.ui.map.model

class Rect(minX: Double, minY: Double, maxX: Double, maxY: Double) {

    var p1: Point = Point(minY, minX)
    var p2: Point = Point(maxY, minX)
    var p3: Point = Point(maxY, maxX)
    var p4: Point = Point(minY, maxX)

}