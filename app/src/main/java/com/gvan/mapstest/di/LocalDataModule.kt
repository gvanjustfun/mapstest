package com.gvan.mapstest.di

import com.gvan.mapstest.data.local_data.LocalDataManager
import com.gvan.mapstest.data.local_data.LocalDataManagerImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalDataModule {

    @Provides
    @Singleton
    fun provideLocalDataManager() : LocalDataManager {
        return LocalDataManagerImpl()
    }

}