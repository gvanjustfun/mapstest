package com.gvan.mapstest.ui.map.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gvan.mapstest.ui.map.model.Rect
import javax.inject.Inject

class MapViewModel @Inject constructor() : ViewModel() {

    lateinit var fieldName: String
    lateinit var rect: Rect

    val fieldNameLiveData = MutableLiveData<String>()
    val fieldRectLiveData = MutableLiveData<Rect>()

    fun init(){
        fieldNameLiveData.value = fieldName
    }

    fun onMapReady() {
        fieldRectLiveData.value = rect
    }

}