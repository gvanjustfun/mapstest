package com.gvan.mapstest.data.model


data class Field constructor(
    val FieldNo: String,
    val pol: String,
    val field_ID: String,
    val FarmName: String,
    val farm_ID: String,
    val CornType: String,
    val FieldNoDescr: String,
    val Area: Double,
    val min_X: Double,
    val max_X: Double,
    val min_Y: Double,
    val max_Y: Double
) {
}