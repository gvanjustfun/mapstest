package com.gvan.mapstest.ui.main.model

import com.gvan.mapstest.data.model.Field


class FieldsData(val fields: List<Field>, val search: Boolean)