package com.gvan.mapstest.data.api

import com.gvan.mapstest.data.model.Field
import retrofit2.http.GET
import retrofit2.http.Query

interface FieldsService {

    @GET("api_esb/AgileRider/api/GetCornFieldsByDevice")
    suspend fun getFields(@Query("dev_ID") devId: String) : List<Field>


}