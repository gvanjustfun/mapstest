package com.gvan.mapstest.data.repository.fields

import com.gvan.mapstest.data.model.Field
import kotlinx.coroutines.flow.Flow

interface FieldsRepository {

 fun getFields(devId: String): Flow<List<Field>>

}