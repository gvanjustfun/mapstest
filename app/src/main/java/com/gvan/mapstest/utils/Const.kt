package com.gvan.mapstest.utils

class Const {

    companion object {
        val FIELD_NAME = "field_name"
        val MAX_X = "max_x"
        val MAX_Y = "max_y"
        val MIN_X = "min_x"
        val MIN_Y = "min_y"
    }

}