package com.gvan.mapstest.ui.main.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.gvan.mapstest.data.model.Field
import com.gvan.mapstest.databinding.ItemFieldBinding
import com.gvan.mapstest.ui.main.list.view_holder.FieldsViewHolder

class FieldsAdapter constructor(private val listener: (Field) -> Unit) : ListAdapter<Field, FieldsViewHolder>(FieldsDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FieldsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemFieldBinding.inflate(inflater, parent, false)
        return FieldsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FieldsViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }
}

class FieldsDiffUtil : DiffUtil.ItemCallback<Field>() {
    override fun areItemsTheSame(oldItem: Field, newItem: Field): Boolean {
        return oldItem.FieldNo == newItem.FieldNo
    }

    override fun areContentsTheSame(oldItem: Field, newItem: Field): Boolean {
        return oldItem == newItem
    }

}