package com.gvan.mapstest.di

import com.gvan.mapstest.data.api.FieldsService
import com.gvan.mapstest.data.repository.fields.FieldsRepository
import com.gvan.mapstest.data.repository.fields.FieldsRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun provideFieldsRepository(fieldsService: FieldsService) : FieldsRepository {
        return FieldsRepositoryImpl(fieldsService)
    }

}