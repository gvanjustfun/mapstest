package com.gvan.mapstest.ui.map

import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolygonOptions
import com.gvan.mapstest.R
import com.gvan.mapstest.databinding.ActivityMapBinding
import com.gvan.mapstest.ui.base.BaseActivity
import com.gvan.mapstest.ui.map.model.Rect
import com.gvan.mapstest.ui.map.view_model.MapViewModel
import com.gvan.mapstest.utils.Const

class MapActivity : BaseActivity<ActivityMapBinding, MapViewModel>(), OnMapReadyCallback {

    var map: GoogleMap? = null

    override fun provideBinding(): Int {
        return R.layout.activity_map
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parseArguments()
        viewModel.init()
    }

    override fun provideViewModel(): Class<MapViewModel> {
        return MapViewModel::class.java
    }

    fun parseArguments() {
        if (intent.extras != null && intent.extras!!.containsKey(Const.FIELD_NAME)) {
            val fieldName = intent.extras!!.getString(Const.FIELD_NAME, "")
            val maxX = intent.extras!!.getDouble(Const.MAX_X)
            val maxY = intent.extras!!.getDouble(Const.MAX_Y)
            val minX = intent.extras!!.getDouble(Const.MIN_X)
            val minY = intent.extras!!.getDouble(Const.MIN_Y)

            viewModel.fieldName = fieldName
            viewModel.rect = Rect(minX, minY, maxX, maxY)
        }
    }

    override fun setupUI() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val mapFragment: SupportMapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun setupViewModel() {
        viewModel.fieldNameLiveData.observe(this, Observer { name ->
            supportActionBar?.title = name
        })

        viewModel.fieldRectLiveData.observe(this, Observer { rect ->
            if(map != null) {
                val p1 = LatLng(rect.p1.lat, rect.p2.lng)
                map!!.moveCamera(CameraUpdateFactory.newLatLngZoom(p1, 12.0f))
                map!!.setMaxZoomPreference(16.0f)

                val polygonOptions = PolygonOptions()
                    .add(LatLng(rect.p1.lat, rect.p2.lng),
                    LatLng(rect.p2.lat, rect.p2.lng),
                    LatLng(rect.p3.lat, rect.p3.lng),
                    LatLng(rect.p4.lat, rect.p4.lng)
                    )
                polygonOptions.strokeColor(ContextCompat.getColor(this, R.color.red))
                polygonOptions.strokeWidth(resources.getDimensionPixelOffset(R.dimen.dist_2).toFloat() )
                map!!.addPolygon(polygonOptions)
            }
        })

    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        viewModel.onMapReady()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}