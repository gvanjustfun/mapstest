package com.gvan.mapstest.data.repository.fields

import com.gvan.mapstest.data.api.FieldsService
import com.gvan.mapstest.data.model.Field
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

//more about flow:
//https://proandroiddev.com/kotlin-flow-on-android-quick-guide-76667e872166

class FieldsRepositoryImpl(private val fieldsService: FieldsService) : FieldsRepository {

    override fun getFields(devId: String): Flow<List<Field>> {
        return flow {
            val fields = fieldsService.getFields(devId)
            emit(fields)
        }.flowOn(Dispatchers.IO)
    }
}